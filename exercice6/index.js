import React    from 'react';
import ReactDOM from 'react-dom';
import App      from './containers/App.jsx';
import TodoApp      from './containers/TodoApp.jsx';

// Render <App /> for static todolist (wanted final result)
// Render <TodoApp /> for the WIP todolist
ReactDOM.render(
    <TodoApp />,
    document.getElementById('root')
);
