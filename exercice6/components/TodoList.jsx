import React from 'react';
import TodoListItem from './TodoListItem.jsx';

export default class TodoList extends React.Component {
    render() {
        var createItem = (itemText)=> {
            return (
                <TodoListItem>{itemText}</TodoListItem>
            );
        };
        return (
            <fieldset>
                <legend>TODO</legend>
                <div className="form-group col-md-10 col-md-offset-1" style={{marginTop: 0}}>
                    {this.props.items.map(createItem)}
                </div>
            </fieldset>
        );
    }
}
