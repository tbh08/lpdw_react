import React from 'react';

export default class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {inputValue: ''};
    }
    onChange(e) {
        this.setState({
            inputValue: e.target.value
        });
    }
    render() {
        return (
            <fieldset>
                <legend>TODO</legend>
                <div className="form-group col-md-10 col-md-offset-1" style={{marginTop: 0}}>
                    <div className="checkbox">
                        <label>
                            <input type="checkbox" /> Pommes
                        </label>
                    </div>
                    <div className="checkbox">
                        <label>
                            <input type="checkbox" /> Poires
                        </label>
                    </div>
                    <div className="checkbox">
                        <label>
                            <input type="checkbox" /> Scoubidou bidou
                        </label>
                    </div>
                </div>
            </fieldset>
        );
    }
}
