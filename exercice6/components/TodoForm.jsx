import React from 'react';

export default class TodoForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {item: ''};
    }
    handleSubmit(e){
        e.preventDefault();
        this.props.onFormSubmit(this.state.item);
        this.setState({item: ''});
        React.findDOMNode(this.refs.item).focus();
    }
    onChange(e){
        this.setState({
            item: e.target.value
        });
    }
    render(){
        return (
            <form onSubmit={this.handleSubmit} className="form-horizontal">
                <div className="form-group">
                <input type='text' ref='item' onChange={this.onChange} value={this.state.item} className="form-control" placeholder="Item to add"/>
                <input type='submit' value='Add' className="btn btn-primary btn-raised"/>
                </div>
            </form>
        );
    }
}
