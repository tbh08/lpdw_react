import React from 'react';

export default class TodoListItem extends React.Component {
    render() {
        return (
            <div className="checkbox">
                <label>
                    <input type="checkbox" /> {this.props.children}
                </label>
            </div>
        );
    }
}
