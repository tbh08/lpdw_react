import React    from 'react';
import Input    from '../components/Input.jsx';
import Header   from '../components/Header.jsx';

export default class App extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-md-offset-3">
                        <header>
                            <Header title="TODO list" />
                        </header>
                        <form className="form-horizontal well">
                            <Input />
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}