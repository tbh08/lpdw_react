import React    from 'react';
import Header   from '../components/Header.jsx';
import TodoList from '../components/TodoList.jsx';
import TodoForm from '../components/TodoForm.jsx';

export default class TodoApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {items: ['Todo item #1', 'Todo item #2']};
    }
    updateItems(newItem) {
        var allItems = this.state.items.concat([newItem]);
        this.setState({
            items: allItems
        });
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 col-md-offset-3">
                        <header>
                            <Header title="TODO list" />
                        </header>
                        <form className="form-horizontal well">
                            <TodoList items={this.state.items}/>
                            <TodoForm onFormSubmit={this.updateItems}/>
                        </form>

                    </div>
                </div>
            </div>
        );
    }
}