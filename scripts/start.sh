#!/usr/bin/env bash

case "$(uname -s)" in

   CYGWIN*|MINGW*|MSYS*)
     ../node_modules/.bin/webpack-dev-server.cmd --progress --colors --config ../webpack.dev.config.js
     ;;

   *)
     ./node_modules/.bin/webpack-dev-server --progress --colors --config webpack.dev.config.js
     ;;
esac

read -p "Appuyer sur <Entrée> pour quitter"
