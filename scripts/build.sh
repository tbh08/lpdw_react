#!/usr/bin/env bash
#Clean
rm -r dist/*
rm -r modules/*

case "$(uname -s)" in

   CYGWIN*|MINGW*|MSYS*)
     #Build JS
     ../node_modules/.bin/webpack.cmd --config ../webpack.prod.config.js && ../node_modules/.bin/babel.cmd src -d modules
     #Build CSS
     ../node_modules/.bin/node-sass.cmd --include-path scss ../src/styles/DEDReactCarousel.scss --output-style compressed  -out-file ../dist/DEDReactCarousel.min.css
     ;;

   *)
    #Build JS
    ../node_modules/.bin/webpack --config ../webpack.prod.config.js && ../node_modules/.bin/babel src -d modules
    #Build CSS
    ../node_modules/.bin/node-sass --include-path scss ../src/styles/DEDReactCarousel.scss --output-style compressed  -out-file ../dist/DEDReactCarousel.min.css
     ;;
esac

read -p "Appuyer sur <Entrée> pour quitter"
