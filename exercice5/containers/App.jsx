import React      from 'react';
import Input  from '../components/Input.jsx';

export default class App extends React.Component {
    render() {
        return (
            <form>
                <Input />
            </form>
        );
    }
}