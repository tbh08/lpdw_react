# Exercice 5 - State et binding

1. Comment puis-je définir le `state` par défaut d'un composant ?
>**Enguerran :** Avec un constructeur :
>```javascript
>constructor(props) {
>   super(props);
>   this.state = {inputValue: ''};
>}
>```

2. Quelle est la particularité du `state` dans un composant React ?
>**Enguerran :** Contrairement à `props` qui est immuable, `state` change dans le temps (ici au changement de la valeur de l'input).

3. Comment puis-je transférer la référence de mon objet (this) à une méthode lorsque j'utilise les attributs natif d'un élément tel que `onChange` ou `onClick` par exemple ?
>**Enguerran :** En utilisant `bind` :
```javascript
    this.Method.bind(this)
```

4. Que ce passe t-il lorsqu'une propriété du `state` d'un composant React est modifiée ?
>**Enguerran :** Lorsque `state` est modifié un nouveau `render()` va être effectué pour rafraichir le composant.
