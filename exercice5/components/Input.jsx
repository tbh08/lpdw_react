import React from 'react';

export default class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = {inputValue: ''};
    }
    onChange(e) {
        this.setState({
            inputValue: e.target.value
        });
    }
    render() {
        return (
            <fieldset>
                <legend>Input</legend>
                <label htmlFor="input-text">Input : </label>
                <input type="text" id="input-text" onChange={this.onChange.bind(this)}/>
                <p className="inputTextValue">Output : <output htmlFor="input-text">{this.state.inputValue}</output></p>
            </fieldset>
        );
    }
}
