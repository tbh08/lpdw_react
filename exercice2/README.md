# Exercice 2 - Questionnaire

1. Dans du JSX, quelles sont les caractères spéciaux utilisés pour déclarer du HTML ? du Javascript ?
> **Enguerran :** Si c'est pour attribuer du code html brute (comme la question 2) à une variable jsx `''` ou `""` seront utilisés. Sinon, il n'y a pas vraiment de caractères spéciaux (sauf `{}` pour le couple attribut-propriété) puisqu'il y a déjà des "factories" pour faire un rendu des éléments html
>
> _Exemple :_
>```javascript
>class MyDomComponent extends React.Component {
>  render() {
>    return React.DOM.div({ className: 'foo' });
>  }
>};
>```
>Pour javascript les charactères `{}` sont utilisés.

2. En JSX, puis-je attribuer du code html tel que
`<h2>Mon Super Titre</h2>` à une variable JSX ? 
> **Enguerran :** oui c'est possible exceptionnellement en utilisant la propriété `dangerouslySetInnerHTML` (mais ce n'est pas recommandé car il n'y a pas d'assainissement de code)
>
> _Exemple :_
>```javascript
>var monSuperTitre = '<h2>Mon Super Titre</h2>';
>render() {
>     return (
>         <div className="content" dangerouslySetInnerHTML={{__html: monSuperTitre}}></div>
>     );
> }
>```