# Exercice 3 - Questions général

Voici une série de question (en vrac) sur ce que nous avons vu en cours. Elle ne sont pas directement liés à l'exercice mais elles peuvent vous aider à mieux comprendre l'environnement dans lequel sont executés les exercices.

1. Pouvez vous m'expliquez la fonction du fichier `index.js` ?
>**Enguerran :** Comme dit dans l'énoncé, le fichier `index.js` le point d'entrée de l'application. Il s'occupe de charger React et les composants créés puis en fait un rendu avec les paramètres que l'on indique (comme `name` ici)

2. Quel est le rôle de webpack (en quelques mots/lignes) ?
>**Enguerran :** Webpack permet de compiler les modules et ses dépendances en bundle d'assets statiques. Il prend en charge un grand nombre de format (js, css, images, less...) et permet également d'utiliser des préprocesseurs (images en base64, less en css minimifié, etc...)

3. Quel est le rôle de babel ?
>**Enguerran :** Comme résumé dans l'énoncé ~~(dans le mauvais sens)~~, babel permet de compiler du javascript en `es2015` à la norme `es5`. Car pour le moment, React ne supporte pas complètement `es2015`.

4. Pourquoi devrais-je utilser tous ces outils alors que je pourrais me contenter d'un bloc-note et d'un navigateur ?
>**Enguerran :** parce que je n'ai pas envie de tondre la pelouse avec un paire de ciseaux

5. Pouvez vous me citez d'autres outils utiles au dévelopement Front et ReactJS en particulier ?
>**Enguerran :** [gulp](https://github.com/gulpjs/gulp) ou [grunt](https://github.com/gruntjs/grunt) qui permettent différentes automatisations via des tâches (comme [livereload](https://github.com/livereload/LiveReload), synchronisation de dossiers, lancement de tests, processing+minimification du sass/less et js puis [dploy](https://github.com/LeanMeanFightingMachine/dploy) pour faire une mise en prod...). Il existe aussi une extension proposée par Facebook pour Chrome (+ autres ?) permettant de débugger ses composants react : [React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)
