# Exercice 1 - Questionnaire

1. Quelles sont, selon vous, les enjeux de l'utilisation de la librairie React au sein d'un projet web ?
> **Enguerran :**  React est une bibliothèque javascript permettant de créer une vue (d'un modèle MVC) ayant des composants dynamiques et réutilisables. Comme son nom l'indique, React est optimisé pour avoir [une vue réactive avec des données qui évoluent rapidement](http://facebook.github.io/react/blog/2013/06/05/why-react.html#reactive-updates-are-dead-simple.).

2. Qu'est-ce que le JSX ? Quelle est sont rôle au sein d'un composant React ?
> **Enguerran :** JSX est un language développé par Facebook qui, avec Instagram, est à l'origine de React. Ce language permet d'écrire du code HTML augmenté directement dans du code Javascript.

3. Quelle est le rôle de la méthode `render` ?
> **Enguerran :** La méthode `render` charge (et recharge) le contenu à afficher (ou plutôt charge une version minimaliste auquelle sera rajouté les styles, etc...)