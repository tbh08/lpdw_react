import React      from 'react';
import Header     from '../components/Header.jsx';

export default class App extends React.Component {
    render() {
        return (
            <header>
                <Header title="Hello World !" />
            </header>
        );
    }
}