# Exercice 4 - Proptypes

1. Quel est l'intêret de passer des propriétés d'un composant à un autre en utilisant les `Proptypes` de React ?
>**Enguerran :** cela permet à la fois de typer la donnée à entrer mais aussi de la rassembler en un seul endroit, le composant principal (ou 'container').

2. Nous avons choisis de passer une propriété `title` de type `string` à l'enfant `Header.jsx`. Pouvez vous me citer d'autres types que l'on pourrait passer en tant que propriété à un composant React ?
>**Enguerran :** `array`,`bool`,`func`,`number`,`object`,`node`(tout ce qui peut être rendu),`element`(un élément réact),une instance de classe : `.instanceOf(Message)`, une énumération:`.oneOf(['News', 'Photos'])`, une énumération avec plusieurs types: `.oneOfType([React.PropTypes.string, React.PropTypes.number])`

3. Lorsque l'on définit un composant React ayant des `props`, il existe un moyen de les ***typer*** et de les rendre ***obligatoire***. Sauriez-vous expliquer ce que `PropTypes` de la librairie React nous propose de faire ?
>**Enguerran :** Il suffit de rajouter `.isRequired`
>
> _Exemple:_ `requiredFunc: React.PropTypes.func.isRequired`

4. Lorsqu'une propriété d'un composant est dites ***non-obligatoire***, quelle serait selon vous le meilleur moyen de définir une valeur par défaut à un composant ? ***Pensez aux `PropTypes` de la lib React.***
```javascript
    getDefaultProps() {
        return {
            value: 'default value'
        };
    }
```